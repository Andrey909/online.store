<?php

namespace App\Http\Controllers;

use App\Product;
use App\Thumbnail;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        $this->validate(request(),[
            'title' => 'required',
            'slug' => 'required|unique:products,slug',
            'price' => 'required|numeric',
            'description' => 'required'
        ]);

        $product = Product::create(request()->all());

        if(count(request()->files->get('thumbnail'))){
            foreach (request()->files->get('thumbnail') as $file){

                $file = $file->move(public_path().'/uploads/', time().'_'.$file->getClientOriginalName());

                $thumbnail = Thumbnail::create([
                    'name' => basename($file->getRealPath()),
                    'size' => basename($file->getSize())
                ]);

                $product->thumbnails()->attach($thumbnail->id);


            }
        }

        return redirect('/products');

    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Product $product)
    {
        $this->validate(request(),[
            'title' => 'required',
            'slug' => 'required|unique:products,slug,'.$product->id,
            'price' => 'required|numeric',
            'description' => 'required'
        ]);

        $product->update(request()->all());

        return redirect('/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return back();
    }
}
