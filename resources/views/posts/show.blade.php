@extends('template')

@section('content')
    <div class="col-md-12">
        <h2>{{ $post['title'] }}</h2>
        <p> {{ $post['body'] }} </p>
    </div>

    <div class="col-md-12">
        <ul class="list-group">

            @foreach($post->comments as $comment)
                <li class="list-group-item">
                    {{ $comment->body }}
                </li>
            @endforeach

        </ul>

        @include('embed.errors')

        <form action="/posts/{{$post->slug}}/comments" method="POST" class="form-horizontal">

            {{csrf_field()}}

            <div class="form-group">
                <label for="comment">Place your comment:</label>
                <textarea name="comment_body" id="comment" class="form-control"></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add comment</button>
            </div>

        </form>
    </div>
@endsection

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">{{ $post['title'] }}</h1>
        </div>
    </div>
@endsection